class CreateBins < ActiveRecord::Migration
  def change
    create_table :bins do |t|
      t.integer :number
      t.string :fiid
      t.integer :count
      t.integer :amount

      t.timestamps null: false
    end
  end
end
