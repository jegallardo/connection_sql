class CreateSwitches < ActiveRecord::Migration
  def change
    create_table :switches do |t|
      t.string :bin
      t.string :router

      t.timestamps null: false
    end
  end
end
